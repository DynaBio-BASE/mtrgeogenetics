
<<blip6, echo=FALSE, results='hide'>>=

# GPS algorithm for Mtr collection, leave-one-out, for distance accuracy
read_chunk('./sources/GPS_LeaveOneOut_LG_PourDraft.R')

# GPS algorithm for Mtr collection, application to unknown accessions
read_chunk('./sources/GPS_functionPubli_updateLG_PourDraft.R')

@ 



 \subsubsection*{Geographic Population Structure of the reference set confirms uneven geographical distribution of \mtr populations.}

 
<<MiseEnPlaceGPS,results='hide', echo=FALSE>>=
@ 

 
We modified the algorithm described by \citet{elhaik_geographic_2014} to adapt it to selfing plants, where genetic distances between different closely related accessions may be identical.
This adaptation takes into account putative ties when defining the closest accession(s) of an unknown sample. 
Ties are also considered when computing the contribution of other reference accessions to the sample's genetic make-up. The reference set is the set of \mtr accessions with reported longitude and latitude. 
It consist in $244$ entries.
We first tested for a correlation between geographical distance and genetic distance. 
Given the (relatively) small distances across the \Mbs, we computed a 'naive' geographical distance using pairwise Euclidean distance based on the longitude/latitude reported for the accessions. 
The matrix of pairwise genetic distances was computed using the proportions of the $8$ admixture components of each accession.
Mantel test of the initial matrices reveals a significant, yet moderate, correlation coefficient between geographical and genetic distances  (r=\Sexpr{round(toto$obs,2)}, p-value=\Sexpr{toto$pvalue}) (\textcolor{red}{compute exact p-values for final draft}).
Supplementary Figure  \ref{fig:MiseEnPlaceGPS} shows that the linear relationship between geographical and genetic distances is restricted to distances less than $950$ km.

When filtering out the distance matrices for distances $> 950$km, the Mantel correlation coefficient raises to \Sexpr{round( toto2$obs, 2 )}, a highly significant value (p-value=\Sexpr{toto2$pvalue}). 
To be able to infer geographical distances from genetic data, we thus estimated the linear relationships between these two quantities for geographical distances less than 950km. The regression equation is $\mathrm{Geo} = \Sexpr{round( eq1$coefficients[1], 3 )} + \Sexpr{round( eq1$coefficients[2], 3 )} \times \mathrm{Gen}  + \epsilon$  with adjusted $R^2=$ \Sexpr{round(summary(eq1)$"adj.r.squared",2)} and model p-value $< 2.2.10^{-16}$.


\begin{suppfigure}[htbp]
  \centering
\includegraphics[width=0.9\textwidth, keepaspectratio=true]{/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/figs/MiseEnPlaceGPS.png}
  \caption{Relation between geographical and genetic distances among $245$ \mtr accessions. \newline The red dotted curve is a loess adjusted curve}
  \label{fig:MiseEnPlaceGPS}
\end{suppfigure}


We then utilized the 'Leave-One out' approach at the 'accession' level to estimate the accuracy of the Geographic Population Structure method. 
That is, we excluded one accession at a time from the reference population set, predicting the biogeography of that accession and calculating their predicted distance from the true origin.
The  mean accuracy is computed per population. 
The modified algorithm was used for this validation step.



\begin{suppfigure}[htbp]
\centering
<<GPSLeaveOneOutFig, fig.width=8, fig.height=8, results='hide', echo=FALSE>>=
@ 
\caption{Predicted distance from true origin for \mtr accessions using the 'leave-one-out' procedure for Geographic Population Structure based genome admixture proportions determined by supervised admixture analysis.}
\label{fig:ecdfGPS}
\end{suppfigure}


<<GPSLeaveOneOutTab, results='asis', echo=FALSE>>=
@ 

The average distance between original and predicted locations is \Sexpr{round(mean(Leave2$distance),0)} km, the median is \Sexpr{round(median(Leave2$distance),0)} km  and 75\% of the samples are predicted less than \Sexpr{round(summary(toto)[5],0)} km from their reported origin (Supplementary Figure \ref{fig:ecdfGPS}).
Typical figures of geographical assignments are shown on Supplementary Figure \ref{fig:LeaveOneOut}. 
Large discrepancies in the prediction accuracy exists among the diverse putative populations (Supplementary Table \ref{tab:MedianDistances}), largely based on the dispersal of a population around the \Mbs.
%The 'Greek genome' has the greater median value, followed by the French Genome. Yet the situations are different. 
The Greek genome has low levels of admixture and is characterized by a wide dispersal at the East of the \Mbs. 
As such, a precise assignment of accessions of 'Greek genome' origin is difficult because closely related genomes are scattered from Italia to Lybia. 
The split of the 'French genome' in two discrete zones (Languedoc and Corsica) is the other pattern that generate large differences between predicted and recorded positions. 
In several cases, Corsica-recorded accessions are affected in Languedoc and vice-versa, generating some heterogeneity in the predictions. 
Predictions of accessions of the 'Spanish Coastal' and 'Spanish-Morocco Inland'' populations are remarkably accurate, whereas these regions span a large geographical area.


\begin{suppfigure}[htbp]
\centering
\subfloat[HM170, 24km][HM170, 24km]{
  \includegraphics[width=0.46\linewidth]{/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/output/figsGPS/Map-HM170_24km}
  \label{fig:miaou1}}
\qquad
\subfloat[HM241, 42km][HM241, 42km]{
  \includegraphics[width=0.46\linewidth]{/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/output/figsGPS/Map-HM241_42km}
  \label{fig:miaou2}}
\\
\subfloat[HM145, 549km][HM145, 549km]{
  \includegraphics[width=0.46\linewidth]{/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/output/figsGPS/Map-HM145_549km}
  \label{fig:miaou3}}
\qquad
\subfloat[HM001, 1489km][HM001, 1489km]{
  \includegraphics[width=0.46\linewidth]{/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/output/figsGPS/Map-HM001_1489km}
  \label{fig:miaou4}}

\caption{Typical examples of Geographic Population Structure analysis, with several levels of accuracy revealed using the 'Leave-one Out' procedure. \newline Sub-captions indicate the distance between observed and predicted positions for the given accession. The known geographical position of the given accession is a bisque circle, the centroid of the closest accessions is a firebrick red square, the inferred position is a red circle. Accessions that participate to the genome make-up are indicated with dashed arrows. A green arrow join the initial centroid to the inferred final position, a black arrow join the original and inferred positions }
\label{fig:LeaveOneOut}
\end{suppfigure}
  

%% \vspace{0.5cm}
%% These data re-enforce the conclusion upon the existence of several distinct ancestral genomes in \mtr. They also clearly illustrate that patterns of repartitions of these genomes are quite uneven, the 'Greek genome' having an unexpected large distribution and the French genome being split into two discrete regions, separated by high levels of admixture with Greek genome on the one hand and the Spanish Coastal genome on the other hand. The Maghreb appears to be a highly structured regions, gathering the majority of the \mtr ancestral genomes. 

%% The split of the 'French genome' may be  due to invasion of 'Greek genome'-originating \mtr accessions, coming through the Italian peninsula via the coast. We can observe that the frequencies of the 'French genome' and of the 'Greek genome' along the Provence coast are never greater that 20\% (Figure \ref{fig:MappingAncestralFigure}). This is a sign of admixture between those two genomes, that may have beneficiated to hybrid accessions to adapt. Admixture may be also promoted by environmental factors. It is well-known that the amount of allogamy within populations depends on temperature and humidity, and one might hypothetized that climate in that particular zone may be more favorable to hybridization.


 
 \subsubsection*{The Jemalong-A17 reference genome might be of 'Spanish Coastal' origin.}

 
<<PredictionInconnues, echo=FALSE, results='asis'>>=
@ 


Using the Geographic Population Structure method, we inferred geographical origin of $17$ non documented accessions (Supplementary Table \ref{tab:Inconnues}).
Among those, the predicted locations of five cultivated varieties of \mtr suggest that their genetic basis may be quite reduced, as $4/5$ are part of the 'Greek' population.
The A17 accession, isolated from the Australian Jemalong cultivar (T. Huguet, personal communication), served as the \mtr reference genome \citep{young_medicago_2011, tang_improved_2014}.
Origin of the Jemalong cultivar is not documented in the literature and official certificate of Australian administration did not clearly state its primary location in the \Mbs. 
The GPS analysis shows that the admixture components of A17 are identical to some accessions belonging to the 'Spanish Coastal population'. 
%Additionaly, the PI442895SSD (HM256) accession is placed in Spain, at the same location to that predicted for the Jemalong-A17 accession. 
%Inspection of online archival records  for that accession reported it as the putative original population of the Jemalong cultivar (Plant Inventory Data record of the US-GRIN, \url{http://sun.ars-grin.gov:8080/npgs_public/prodweb.pdf0?in_vol=188&in_suffix=pt1&in_page=507}). 
Applying the GPS method, we determined a likely original geographical position of the Jemalong-A17 reference accession as illustrated Figure \ref{fig:CartosInconnues}. 
This location within the Spanish Coastal population is in accordance with the resistant phenotype of A17 in response to \va \citep{ben_natural_2013}. 
These independent results provide additional evidence of the role of admixture components in determining phenotypes.


<<CartosInconnues, fig.cap='Predicted geographical location of the \\mtr reference accession A17, using the GPS algorithm. \\newline Closest accessions with reported geographical location are displayed in cyan. The predicted location is the centroid of the closest accessions, weighted by their genetic distance to A17.', fig.scap='Predicted geographical location of the \\mtr reference  accession A17, using the GPS algorithm. \\newline Closest accessions with reported geographical location are displayed in cyan. The predicted location is the centroid of the closest accessions, weighted by their genetic distance to A17.', echo=FALSE, results='hide', fig.width=18, fig.height=8>>=
@ 

This short investigation shows the potential of GPS method in locating unknown plant samples based on their admixture components, and might have some applications in forensic sciences and technologies.


%% The  PI564941SSD (HM262) accession is placed in Morrocco, in accordance with partial record of its origin \textsl{[pdf Supplemental Bibliography ?]}. 
%% As a consequence, their location is quite imprecise and corresponds to that described in the official passport data (see Supplemental Bibliography) only for three of them. %Parragio (HM208, 'Greek genome') is misplaced : described as originating from Reggio di Calabria in Italy, it is estimated near Syria. 
%% %Sephi (HM209, 'Greek genome') is misplaced : described as collected  near Mt. Meron, Upper Galilee, Israel and estimated in South of Italy. Cyprus (HM115, 'Greek genome') : collected in Cyprus and is located in that region. 
%% Borung (HM019, 'Atlas' genome) reported to be collected between Le Kef and Le Krib, Tunisia, is accurately placed. Intriguingly, Caliph (HM207, 'Greek genome), described as a back-crossing program using Cyprus as the recurrent parent, is not located as the same place as Cyprus.




