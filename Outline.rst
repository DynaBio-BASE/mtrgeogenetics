
Quels sont les résultats majeurs ? :
====================================
En s'appuyant sur les données de >840K SNPs, on montre que : 


\subsection*{\texttt{ADMIXTURE} analysis suggests height  admixture components in \Mtr.}


\subsection*{\mtr admixture components are related to geographical and bioclimatic variables.}


\subsection*{Admixture can explain phenotypic patterns in the \Mbs.} 


\subsection*{Partial resistance  to \Va is related to particular genome components, allowing prediction of phenotype. }


\subsection*{Application of GPS to plants shows that genetics and geographics are correlated. }

 \subsubsection*{Geographic Population Structure of the reference set.}

 \subsubsection*{The Jemalong-A17 reference genome might be of 'Spanish Coastal' origin.}




Donc, éléménts de question pour l'abstract:
===========================================
Nous nous proposons de déterminer si 






Remarques EE sur abstract/intro :
=================================

This is the method, but what is the question it purports to answer? just unclear structure is not a question. surely there is some hypothesis and if not pick one from the result. for example, can admixture explain phenotypic patterns in the ME bassin?


This is not intersting. what is interesting is how accurately could you predict the origin of plants given a test sample of anonymous origin? can this be used in murder trial if you find the leaves in someone shoes? if it works, need to say that this is the first application of GPS to plants which shows that genetics and geographics are correlated and can be used to predict one another as well as to phenotype and thereby can predict future trends.


Sounds like there is a lack of hypotheses about this data. Maybe some ideas:

1. Where did this plant start to spread? (can be answered with dating analysis and following admixture patterns from k=2 and up which you should probably present).
2. Why phentyope X has this particular distribution?3. Why the plant is only found around the ME and now outside of it?
4. Do self-polinating plants still exhibit admixture patterns?


the population structure is inferred from admixture, not GPS.
GPS gives you biogeographical origin, when you have a question to ask. it doesn't seem like you asked GPS anything you just showed that it has good predictions to something you already know, so why use it? 
