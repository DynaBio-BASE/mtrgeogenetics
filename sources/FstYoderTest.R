

# note pour effacer les objets de l'espace de travail
rm(list = ls())
graphics.off()


## ---- FstSummaries1 ----

 Dir <- "/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/Manuscript/"
Dir2 <- "/home/gentz/Documents_Recherche/Textdocuments/M_truncatula_programmes_projets/USC_Docs/Tanya_Collab/GPS/PreliminaryResults/"


FstTouT <- read.table(paste(Dir,"/data/Fst_by_gene_and_condition_840.csv",sep=""),header=TRUE)

FstYoderGenes <- read.table(paste(Dir,"/data/Fst_by_gene_putative_effect_840.csv",sep=""),header=TRUE)

Coordonnees <- read.table(paste(Dir2,"/output/MartinOutputs/gene_list.csv",sep=""),header=FALSE, col.names=c("chrom","start","end","ID"))



#x11()
#hist(FstTouT$AVG_FST)

toto <- FstTouT[order(FstTouT$AVG_FST, decreasing=TRUE),]
head(toto[,c('GENE_NAME','AVG_FST','N_SNPs')], 60)

tata <- merge(toto,Coordonnees, by.x="GENE_NAME", by.y="ID")

write.table(tata[order(tata$AVG_FST, decreasing=TRUE), ], paste(Dir2,"/output/FstToutOrdered.csv",sep=""), sep=";", row.names=FALSE)

#x11()
#hist(FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='AMT'])

#table(FstYoderGenes$VARIABLE)
#table(FstYoderGenes$GENE_NAME, FstYoderGenes$VARIABLE)


(test1 <- wilcox.test(FstTouT$AVG_FST,FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='AMT'] ) )
(test2 <- wilcox.test(FstTouT$AVG_FST,FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='PWM'] ) )
(test3 <- wilcox.test(FstTouT$AVG_FST,FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='ITH'] ) )

#median(FstTouT$AVG_FST,na.rm=TRUE)
#median(FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='AMT'],na.rm=TRUE)
#median(FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='PWM'],na.rm=TRUE)
#median(FstYoderGenes$AVG_FST[FstYoderGenes$VARIABLE=='ITH'],na.rm=TRUE)

## ---- finFstSummaries1 ----





 
