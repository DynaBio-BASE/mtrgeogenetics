
1. présentation générale adapatation, local adaptation, etc ...

Adaptation to local environments often occurs through natural selection acting on a large number of alleles, each having a weak phenotypic effect. 
One way to detect these alleles is to identify genetic polymorphisms that exhibit high correlation with environmental variables used as proxies for ecological pressures.
Correlations between some phenotype and 'climatic' variables (moist, temperature ?), that allow evidencing for a genetic basis for climate adaptations in A. thaliana (as described in Hancock et al. 2011).

Genetic admixture occurs when individuals from two or more previously separated populations begin interbreeding. Admixture results in the introduction of new genetic lineages into a population. It has been known to slow local adaptation by introducing foreign, unadapted genotypes (known as gene swamping)
An admixed population arises when mating occurs between individuals from reproductively isolated ancestral populations.
Populations can evolve more or less independently. This
depends on the number of individual migrations occurring
a
mong them and determines how genetically distinct
the populations are. In some instances, populations which
have evolved separately for a long time come into contact
producing a hybrid population, the genes of which are a
mixture of the parental populations (Bernstein 1931).

All human populations exhibit some level of genetic differentiation. This differentiation, or population stratification, has many interacting sources, including historical migrations, population isolation over time, genetic drift, and selection and adaptation. If differentiated populations remained isolated from each other over a long period of time such that there is no mating of individuals between those populations, then some level of global consanguinity within those populations will lead to the formation of gene pools that will become more and more distinct over time. Global genetic differentiation of this sort can lead to overt phenotypic differences between populations if phenotypically relevant variants either arise uniquely within those populations or begin to exhibit frequency differences across the populations. This can occur at the single variant level for monogenic phenotypes or at the level of aggregate variant frequency differences across the many loci that contribute to a phenotype with a multifactorial or polygenic basis. However, if individuals begin to interbreed (or 'admix') from populations with different frequencies of phenotypically relevant genetic variants, then these admixed individuals will exhibit the phenotype to varying degrees. The level of phenotypic expression will depend on the degree to which the admixed individuals have inherited causative variants that have descended from the ancestral population in which those variants were present (or, more likely, simply more frequent).


2. medicago, its biology, its genomic ressources....
\Mtr originated from the \Mbs and was subsequently naturalized in Australia as a pasture (ref.), and in California as a weed along with other \textit{Medicago} species such as \textit{M. polymorpha} and \textit{M. dsdsd.} that is considered as an invasive species.

put here references that describe selfing rates, from 0.70 to $\geq$ 97\%.




Population structure  of \mtr  remains unsatisfactorily established, with often contradictory structures described  : 
\citep{ellwood_ssr_2006, lazrek_use_2009, bonhomme_high-density_2013, ronfort_microsatellite_2006, badri_quantitative_2007}

de Mita et al. 2011 :
That remains a theoretical challenge.
We observed a broad population structure (K = 2) con-
sistent with previous analysis of SSR data [20] which, in
the absence of obvious barrier to gene flow, seems likely
a relic of past isolation reflecting the existence of glacial
refuges both West (Spain) and East (Levant) of the Medi-
terranean basin. Interestingly clear footprints of selection
were detected only in one of the two groups (the western
group 2) suggesting that selective pressures differ across
Western and Eastern parts of the species range. No
obvious environmental differences between these two
geographic regions could be identified: both regions span
a similar latitudinal gradient and share the mosaic of cli-
mate, geology and anthropic pressures that characterize
the Mediterranean [50]. The only notable difference
between these two groups is the size of the geographic
area they span, the Western group being restricted to the
Iberian Peninsula and Northern Morocco.

transition vers la question :
Strongly structured populations could obscure genome wide association studies inferences when the infinitesimal model of genetic variation holds. 
It is thus required to infer that structure with high precision, which has not been done before for most plants.


3. the question :

Lots of population genetic analysis assume a unique origin with stepping stone divergence patterns.
We are taking a different approach by assuming that initial founders of \Mtr have diverged over large area, adapted to different conditions, built in numbers, and then encountered the secondary contact/admixture zones.
It is difficult to reconstruct the patterns of such scenario unambiguously, but the careful analysis below is making such an attempt combining PCA, admixture, and GPS tools.

Glacial refugia hypothesis : Mtr was split into two separated populations, one in Spain/Marocco/W of Algeria, the other in Greece; that  encountered subsequent admixture.

species with similar distributions. What we refer to as
the ‘Pleistocene Paradigm’ describes the genetic structure
resulting from periodic north/south movements in response
to the expansion and contraction of available habitat during
the Pleistocene glacial cycles. During glacial advance and
particularly during glacial maxima, genetic drift acted to
alter allele frequencies in individual refugia, eventually
leading to genetic divergence among them. As glaciers
retreated, areas of now suitable habitat to the north were
colonized by individuals from adjacent southern refugia,
leading to an east–west pattern, or one in which zones of
differing allele frequencies are encountered as one moves
from east to west across the range (Marmi et al. 2006; Skrede
et al. 2006; Zink et al. 2006). The recolonization process itself
is thought to produce a second gradient, one of decreased
genetic diversity in the recently colonized northern portion
of the range relative to the southern refugia (Comes \&
Kadereit 1998).
Human-mediated long distance dispersal can also
(IBD). This pattern is driven by frequent gene flow between
neighbouring populations relative to that between distant
ones (Kimura \& Weiss 1964), and is the result of both this
‘stepping-stone’ model of gene flow and drift/gene-flow
equilibrium (Kimura \& Weiss 1964; Hutchinson \& Templeton
1999). Since A. thaliana primarily undergoes self-fertilization
(Abbott \& Gomes 1989), the potential for gene flow via pollen
is extremely limited and violation of the stepping-stone
model would be due to long distance seed dispersal. An
intriguing possibility is that higher levels of human activity
(Sanderson et al. 2002) in the European portion of A. thaliana’s
range could lead to more frequent episodes of long distance
dispersal, and therefore reduced levels of IBD. Intentional
human dispersal has been implicated in previous population
genetic studies of European taxa such as wild cherry (Prunus
avium, Mohanty et al. 2001), the brown hare (Lepus euro-
paeus, Kasapidis et al. 2005), and perennial ryegrass (Lolium
perenne, -Warren et al. 1998). Such human dispersal would
be almost assuredly unintentional in an organism such as A.
thaliana, which was until recently of little human interest.


In order to evaluate the hypothesis of genetic differentia-
tion between proposed refugia, the significance of allelic
differentiation and pairwise FST between pairs of refugia
was assessed in arlequin 3.0. Proposed refugia included
the Iberian, Italian, and Balkan Peninsulas, the Caucasus
region, and Central Asia (samples from modern day
Tajikistan, Uzbekistan, and southern Kazakhstan).


4. résumé ect ...


Attemtp to explain that we used 'simple' method based on linear models to detect assocaitions between admixture compoenants and variables. Other methods that might be used are those described in Hancock et al. 2011 or the use of BayEnv (Coop et al. 2010).


novelty  : use of (admixture methods and) GPS to assess the optimum number of populations.
           use admixture proportions, instead of raw SNPs, to test for relations between genome variations and explanatory variables and key phenotypes
