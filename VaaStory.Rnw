
%% <<set-parent, echo=FALSE, cache=FALSE>>=
%% set_parent('./MtrGPS_DraftV0.Rnw')
%% @

<<blip4, echo=FALSE, results='hide'>>=
# Verticillium partial resistance in the set of Hapmap accessions.
read_chunk('./sources/AnaResistance_Va_HapMap.R')

# Verticillium partial resistance : inferred and observed in a set of 'unknown' accessions.
read_chunk('./sources/CartoResistance_Va_HapMap-VerifExpe.R')
@ 


 \mtr is prone to the infection by the soil-borne fungus \Va. 
 Both plant and fungal species are co-occuring around the \Mbs (CABI database, PlantWise database \url{http://www.plantwise.org/}, Oct,25 2014). 
 The Maximum Symptom Score (MSS) of $242$ \mtr accessions when infected with the V31-2 strain of \va are reported Supplementary Table \ref{tab:MSSHapMap}\textcolor{red}{[file]}, on a scale from $0$ for plants without symptoms to $4$ for dead plants \citep{ben_natural_2013, negahi_quantitative_2014}. 
 

<<CartoMSSHapMap, fig.cap = "Geographical repartition of Maximum Symptom Score (MSS) in response to \\Va in a collection of 242 \\mtr accessions.\\newline The MSS scale from 0 (healthy plant after infection) to 4 (dead plant after infection) and is displayed as a color gradient. Scale of index from resistant (blue) to susceptible (red) accessions is indicated on the right.\\newline Admixture proportions of each phenotyped accessions are summarised by pie charts.", fig.scap = "Geographical repartition of Maximum Symptom Score (MSS) in response to \\Va in a collection of 242 \\mtr accessions.\\newline The MSS scale from 0 (healthy plant after infection) to 4 (dead plant after infection) and is displayed as a color gradient. Scale of index from resistant (blue) to susceptible (red) accessions is indicated on the right.\\newline Admixture proportions of each phenotyped accessions are summarised by pie charts.", out.width='1\\linewidth', fig.pos='htbp', fig.width=14, fig.height=8, echo=FALSE, results='hide'>>=
@ 

<<modelMSSHapMap, results='asis',echo=FALSE>>=
@ 

Figure \ref{fig:CartoMSSHapMap} depicts the geographical repartition of MSS index, together with the admixture patterns of the accessions. 
It appears that accessions located West of the \Mbs are mainly resistant to the V31-2 strain, while accessions located East of the \Mbs are susceptible.
We then tested if some genome components might be related to partial resistance to \Va.
Table \ref{tab:ModeleVerti} shows that the proportions of four admixture components  are significantly related to MSS, illuminating the data of Figure \ref{fig:CartoMSSHapMap} (r$^2 = \Sexpr{round(toto$adj.r.squared,2)}$, p-value$ \leq \numprint{2.2e-16}$).
The average MSS score of ``Spanish Coastal'', ``Spanish-Morocco Inland'' and ``South Tunisian Coastal'' genome components are $2.45-1.41= 1.04$, $2.45-0.85=1.6$ and $2.45-0.72=1.71$ respectively, making them resistant genotypes.
The average MSS score of ``Greek'' genome component is $2.45+.60 \simeq 3$, making it a clearly susceptible genotype.
An interesting admixture pattern occurs in Israel/Jordania/Lebanon region. At that place, accessions are admixture between ``Greek'' and ``South Tunisian Coastal'' genomes and exhibit intermediate phenotypes (Figure \ref{fig:CartoMSSHapMap}).

%% \vspace{0.5cm}
We decided to validate these results using an independent set of accessions that were not previously re-sequenced. Making the guess that closely located accessions harbor similar genome components, we predicted their phenotype as follows : uncharacterized accessions located within the geographic zone of partially resistant (respectively susceptible) are predicted partially resistant (respectively susceptible). We decided to sample $29$  new accessions of the `Spanish Coastal' or `Spanish-Morocco' geographic zone, as well as $30$ accessions of the `Greek' geographic zone (Figure \ref{fig:LocUnknown}).  The MSS of these accessions when infected by V31-2 strain of \va are reported Supplementary Table \ref{tab:MSSMtpSup} \textcolor{red}{[file]}. 


<<CodeVa1, echo=FALSE, results='hide'>>=
@ 


\begin{figure}[htbp]
  \centering
  \subfloat[First sub-figure]{\label{fig:LocUnknown}%
<<FigureWhereUnknowns, fig.width=14, fig.height=8, echo=FALSE, results='hide'>>=
@
    }\\
  \subfloat[First sub-figure]{\label{fig:ObsMSS}%
<<PredictedObserved, echo=FALSE, result='hide', fig.width=14, fig.height=8>>=
@
    }
    \caption{Predicting and validating phenotypes based on  admixture component relationship with partial resistance to \va in \mtr. \newline (a) Sampling of uncharacterized accessions in the geographic zone of the 'Spanish Coastal' and 'Spanish-Morocco Inland' resistant populations,  and sampling of uncharacterized accessions in the geographic zone of the 'Greek' susceptible populations. The MSS of reference accessions is displayed as a color gradient (see Figure \ref{fig:CartoMSSHapMap}). (b) Violin plots of MSS for reference and sampled accessions.}
    \label{fig:ValidVerti}
\end{figure}

  
<<TukeyObsPredits, echo=FALSE, results='asis'>>=
@ 


Figure \ref{fig:ObsMSS} shows the phenotypic scores of hypothesized resistant or susceptible accessions, compared to those of resistant or susceptible reference accessions. 
When comparing to the phenotypic scores of reference resistant or susceptible populations, ANOVA shows that it exists very significant differences in MSS among the sets of accessions (p-value $< \numprint{2e-16}$). 
It also indicates that predicted resistant accessions are not significantly different form the reference resistant populations (adjusted p-value$=0.42$) and that predicted susceptible accessions are not significantly different form the reference susceptible population (adjusted p-value$=0.77$) (Table \ref{tab:CompareMSSObsPredits}).
Defining $MSS=2$ as a threshold for resistant (below) or susceptible (above) accessions, we found that $21/29$ predicted accessions are in fact resistant, and that $24/30$ predicted susceptible accessions are susceptible ($\chi^2 = 14.28$,  p-value $= 0.00016$).

These results re-enforce the conclusion that QDR in plants may be provided by a putatively large number of genes scattered throughout the genome. Patterns of resistances/susceptibility are also clearly correlated with particular genome components. Because of the co-occurrence of both the plant species and the pathogen, it is reasonable to hypothesis that the observed pattern of partial resistance in the  \mtr/\va pathosystem may be due to natural selection at least, with unknown contributions of drift and migration.


